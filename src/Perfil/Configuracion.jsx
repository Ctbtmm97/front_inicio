import react, { useState, useEffect } from "react";
import { NavLink, Link,Redirect } from "react-router-dom";
import { Collapse, Navbar, Nav, NavbarToggler, Form, FormGroup, Label, Input, FormText, Container, Col, Row, Button } from 'reactstrap';
import styled from 'styled-components';
import NavBarPerfil from "../Elementos/NavBarPerfil.jsx";
import { withCookies } from "react-cookie";
import Controller from "../BienvenidaController";

const FotoPerfil = styled.div`
border-radius:50%;
width:150px;
height:150px;
box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
display:inline-block;
margin:30px;
background-size: cover;
background-position:center;
background-image:url(${props => props.imgSrc});

`;

const Labels = styled.h4`
font-family: 'Architects Daughter', cursive;
`;

const DescripcionPagina = styled.h1`
font-family: 'Architects Daughter', cursive;
padding:15px;
`;


const Configuracion = (props) => {

    //Variables del NavBar
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    const pais = props.cookies.get("Compitrueno_paisElegido");
    const nombre = props.cookies.get("Compitrueno_nombreUsuario");
    const apellidos = props.cookies.get("Compitrueno_apellidosUsuario");
    const email = props.cookies.get("Compitrueno_emailUsuario");
    const contrasenya = props.cookies.get("Compitrueno_contrasenyaUsuario");
    const valoracion = props.cookies.get("Compitrueno_valoracionUsuario");
    const fecha = props.cookies.get("Compitrueno_fechaUsuario");
    const id = props.cookies.get("Compitrueno_idUsuario");
    const foto = props.cookies.get("Compitrueno_fotoUsuario");
    const idPais = props.cookies.get("Compitrueno_idPaisElegido");

    const [datosFotos, setDatosFotos] = useState([]);
    const [error, setError] = useState('');

    const url_perfil = `/perfil/${id}`;
    
    const [volver, setVolver] = useState(false);
    const [nombreEditado, setNombreEditado] = useState("");
    const [apellidosEditado, setApellidosEditado] = useState("");
    const [cumpleEditado, setCumpleEditado] = useState("");

    useEffect(() => {
        Controller.getFotos(id)
            .then(data => {
                if (data.ok) {
                    setDatosFotos(data.data);
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));

    }, [])

    const API_FOTOS = 'http://localhost:3000/img/';

    const Publicar = () => {
        const PerfilEditado = {
            id: id,
            nombre: nombreEditado,
            apellidos: apellidosEditado,
            nacimiento: cumpleEditado
        };
        props.cookies.set("Compitrueno_fechaUsuario",cumpleEditado, { path: "/" });
        props.cookies.set("Compitrueno_nombreUsuario", nombreEditado, { path: "/" });
        props.cookies.set("Compitrueno_apellidosUsuario",apellidosEditado,{path:"/"});

        Controller.editarPerfil(PerfilEditado);
        setVolver(true);
    }
    if (volver) {
        return (
            <Redirect to={url_perfil} />
        )
    }

    return (
        <>

            <NavBarPerfil pais={pais} />

            <Container className=" estContainer ">
                <DescripcionPagina>Edita tu perfil</DescripcionPagina>
                <hr />
                <Row className="mt-5">
                    <Col className="col-lg-4 col-12">
                        <FotoPerfil imgSrc={API_FOTOS + foto} />
                        <FormGroup>

                            <Input type="file" name="file" id="FotoPerfil" />
                            <FormText color="muted">
                                Procura que tu foto de perfil  sea  clara e individual donde todo el mundo pueda reconocerte correctamente. Gracias por contribuir al bienestar de la página.
                            </FormText>
                        </FormGroup>

                    </Col>
                    <Col className="col-lg-8 col-12">
                        <Form>
                            <FormGroup>
                                <Label for="Nombre"><Labels>Nombre</Labels></Label>
                                <Input type="text" id="Nombre" placeholder={nombre} onChange={e => setNombreEditado(e.target.value)} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="Apellidos"><Labels>Apellidos</Labels></Label>
                                <Input type="text" id="Apellidos" placeholder={apellidos} onChange={e => setApellidosEditado(e.target.value)} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="Cumple"><Labels>Fecha Nacimiento: </Labels> </Label>
                                <Input type="date" name="cumple" id="examplePassword" placeholder={fecha} onChange={e => setCumpleEditado(e.target.value)} />
                            </FormGroup>

                            <Button className="m-3" onClick={Publicar}>Enviar cambios</Button>
                        </Form>


                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default withCookies(Configuracion)