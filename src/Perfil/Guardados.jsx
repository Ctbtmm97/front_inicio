import React, { useState, useEffect } from "react";
import { NavLink, Link, } from "react-router-dom";
import styled from 'styled-components';
import StarsRating from "stars-rating";
import {
    
    Container,
    Col,
    Row,

    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText,
    Button
} from 'reactstrap';

import PerfectScrollbar from 'react-perfect-scrollbar'
import 'react-perfect-scrollbar/dist/css/styles.css';

import prueba from "../prueba.json";
import { withCookies } from "react-cookie";
import NavBarPais from "../Elementos/NavBarPais.jsx";
import Controller from "../BienvenidaController";


const FotoPerfilNav = styled.div`
border-radius:50%;
border:2px solid black;
width:50px;
height:50px;
display:inline-block;
background-size: cover;
background-position:center;
background-image:url("https://e7.pngegg.com/pngimages/43/926/png-clipart-computer-icons-avatar-user-avatar-heroes-woman.png")

`
    ;
// F8EBFF
const PerfilUsuario = styled.div`

border:2px solid #6F0CBB;
border-radius:20px;
background-color:#FFFAEB;
margin:5px;
padding:10px;
text-align:center;
position:relative;

`;
const ImagenPerfil = styled.div`

width: 150px;
height: 150px;
border: 2px solid #6F0CBB;
display:inline-block;
border-radius: 200px;
background-image: url( ${props => props.imgSrc});
background-size:cover;
background-position:center;


`;

const FechaViajero = styled.div`

margin-left:5px;
margin-right:10px;


`;


const DescripcionPagina = styled.h1`
font-family: 'Architects Daughter', cursive;
padding:15px;
`;
const DescPagPequeña = styled.h4`
font-family: 'Architects Daughter', cursive;
margin-top:15px;
`;
const EurosPresupuesto = styled.div`

display:inline-block;
padding:7px;

`;

const FechaViaje = styled.span`
font-size:20px;
padding-right:10px;
`;


const Guardados = (props) => {

    const pais = props.cookies.get("Compitrueno_paisElegido");
    const idPais = props.cookies.get("Compitrueno_idPaisElegido");
    const id = props.cookies.get("Compitrueno_idUsuario");
    const foto = props.cookies.get("Compitrueno_fotoUsuario");

    //DEL NAVBAR


    //Cambiar al id que cojas con el ge
    const url_viajeros = `/inicio/${pais}/viajeros`;

    const url_otroPerfil = `/inicio/perfil/`;

    const [viajeras, setViajeras] = useState([]);
    const [anfitrionas, setAnfitrionas] = useState([]);
    const [error, setError] = useState('');

    const API_FOTOS = 'http://localhost:3000/img/';

    useEffect(() => {

        Controller.getViajerosGuardados(id)
            .then(data => {
                if (data.ok) {
                    setViajeras(data.data);
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));

        Controller.getAnfitrionasGuardadas(id)
            .then(data => {
                if (data.ok) {
                    setAnfitrionas(data.data);
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));

    }, [])

    const Presupuesto = (x) => {
        if (x == 1) {
            return (<i class="fa fa-eur" style={{ color: "green" }} aria-hidden="true" />);
        }
        else if (x == 2) {
            return (<><i class="fa fa-eur" aria-hidden="true" /> <i class="fa fa-eur" aria-hidden="true" /></>);
        }
        else return (<><i class="fa fa-eur" style={{ color: "#B61818", margin: "2px" }} aria-hidden="true" /><i class="fa fa-eur" style={{ color: "#B61818", margin: "2px" }} aria-hidden="true" /><i class="fa fa-eur" style={{ color: "#B61818", margin: "2px" }} aria-hidden="true" /></>);

    }

    function calculateAge(birthday) {
        var ageDifMs = Date.now() - birthday;
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    const DatosViajeras = viajeras
        .map((el) => {
            let edad = calculateAge(new Date(el.nacimiento));
            return (
                <Col className="col-lg-3 col-md-6 col-sm-12 p-1" >
                    <PerfilUsuario >
                        <ImagenPerfil imgSrc={API_FOTOS + el.foto} />
                        <h4> <Link style={{ color: '#6206AA' }} to={url_otroPerfil + el.id}>{el.nombre + ", " + edad}</Link></h4>
                        <div style={{ display: "inline-block", verticalAlign: "bottom" }}>
                            <StarsRating
                                count={5}
                                value={el.estrellitas}
                                size={24}
                                edit={false}
                                color2={'#ffd700'} /></div>

                        <FechaViajero>
                            <FechaViaje style={{ display: "inline-block" }}><strong>
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>{" " + el.fecha}</strong>
                            </FechaViaje>
                        </FechaViajero>
                        <EurosPresupuesto>{Presupuesto(el.presupuesto)}</EurosPresupuesto>
                        <PerfectScrollbar style={{ height: "100px" }}>
                            <p>{el.descripcion}</p>
                        </PerfectScrollbar>
                    </PerfilUsuario >
                </Col>
            )
        });

    const DatosAnfitrionas = anfitrionas
        .map((el) => {

            let edad = calculateAge(new Date(el.nacimiento));

            return (
                <Col className="col-lg-3 col-md-6 col-sm-12 p-1">
                    <PerfilUsuario>
                        <ImagenPerfil imgSrc={API_FOTOS + el.foto} />
                        <h4> <Link style={{ color: '#6206AA' }} to={url_otroPerfil + el.nombre}>{el.nombre + ", " + edad}</Link></h4>
                        <div style={{ display: "inline-block", verticalAlign: "bottom" }}>
                            <h5>{el.ciudad}</h5>
                            <StarsRating
                                count={5}
                                value={el.estrellitas}
                                size={24}
                                edit={false}
                                color2={'#ffd700'} /></div>
                        <PerfectScrollbar style={{ height: "100px" }}>
                            <p>{el.descripcion}</p>
                        </PerfectScrollbar>
                    </PerfilUsuario >
                </Col>)
        });

    return (
        <>
            <NavBarPais pais={pais} id={id} foto={foto} />

            <Container  className=" estContainer " style={{ position: "relative" }} >
                <DescripcionPagina>Viajeras Guardadas</DescripcionPagina>
                <hr />

                <Row className="mt-5">
                    {DatosViajeras}
                </Row>
                <DescripcionPagina>Anfitrionas Guardadas</DescripcionPagina>
                <hr />
                <Row className="mt-5">
                    {DatosAnfitrionas}
                </Row>
            </Container>


        </>

    );
}

export default withCookies(Guardados);