import react, { useState, useEffect } from "react";
import { NavLink, Link } from "react-router-dom";
import {
    Collapse,
    Navbar,
    Nav,
    NavbarToggler,
    Button,
    Container,
    Col,
    Row
} from 'reactstrap';
import prueba from '../prueba.json';

import styled from "styled-components";

import StarsRating from "stars-rating";
import Controller from "../BienvenidaController"

import NavBarPais from '../Elementos/NavBarPais.jsx';
import { withCookies } from "react-cookie";

import RayoFondo from '../Fotos/RayoBien.png';



const Divs = styled.div`
width:100%;
height:100%;
padding:20px;
align-items:center;

`;

const DivPublis = styled.div`
width:100%;
height:100%;
padding:20px;
align-items:center;
hoover:active;
border-radius:10px;
&:hover{
    box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
    
}
position:relative;



`;



const FotoPerfil = styled.div`
border-radius:50%;
width:200px;
height:200px;
display:inline-block;
box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
margin:30px;
background-size: cover;
background-position:center;
background-image:url(${props => props.imgSrc})
`
    ;

const FotoPerfilNav2 = styled.div`
border-radius:50%;
width:50px;
height:50px;
display:inline-block;
margin:30px;
margin-top: 0px;
background-size: cover;
background-position:center;
background-image:url(${props => props.imgSrc});
`
    ;

const DivFotoPrueba = styled.div`
  width:100%;
height:100%;
border:1px solid black;
padding:20px;
background-size: cover;
background-position:center;
background-image:url(${props => props.foto});
justify-content:center;

`;

const DescPagH4 = styled.h4`
font-family: 'Architects Daughter', cursive;
margin-top:15px;
`;

const DescPagH2 = styled.h2`
font-family: 'Architects Daughter', cursive;
margin-top:15px;
`;
const DescPagH1 = styled.h1`
font-family: 'Architects Daughter', cursive;
padding:15px;
`;

const ContainerPrueba=styled.div`
background-color:white;
border-right:10px solid black;
border-left:10px solid black;
`;
const Fondo=styled.div`

width:100%;
height:100%;
;


`;


const PerfilOtro = (props) => {

    const [datosReviews, setDatosReviews] = useState([]);
    const [datosComentarios, setDatosComentarios] = useState([]);
    const [datosPublicacionesA, setDatosPublicacionesA] = useState([]);
    const [datosPublicacionesV, setDatosPublicacionesV] = useState([]);
    const [datosFotos, setDatosFotos] = useState([]);
    const [nombreUsu, setNombreUsu] = useState("");
    const [nacimientoUsu,setNacimientoUsu]=useState("");
    const [fotoUsu,setFotoUsu]=useState("");

    const [error, setError] = useState('');

    const [estrellitas, setEstrellitas] = useState([]);




    //COOKIES
    const pais = props.cookies.get("Compitrueno_paisElegido");
    const id = props.cookies.get("Compitrueno_idUsuario");
    const foto = props.cookies.get("Compitrueno_fotoUsuario");
    const idAux = props.match.params;
    const idOtro = idAux.id1;

    const API_FOTOS = 'http://localhost:3000/img/';

    const url_perfil = `/perfil/${id}`;

    function calculateAge(birthday) {
        var ageDifMs = Date.now() - birthday;
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }



    //SACO LAS EXPERIENCIAS 


    useEffect(() => {

        Controller.getUsuarioById(idOtro)
            .then(({ data }) => {
                setNombreUsu(data.nombre);
                setNacimientoUsu(data.nacimiento);
                setFotoUsu(data.foto);
            })

        Controller.getExperienciasById(idOtro)
            .then(data => {
                if (data.ok) {
                    setDatosReviews(data.data);
                    console.log(datosReviews)
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));


        //SACO MIS COMENTARIOS 


        Controller.getComentRecibidos(idOtro)
            .then(data => {
                if (data.ok) {
                    setDatosComentarios(data.data);
                    console.log(datosComentarios)
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));


        //SACO MIS PUBLICACIONES

        //ANFITRIÓN



        Controller.getMyPostAnfitriona(idOtro)
            .then(data => {
                if (data.ok) {
                    setDatosPublicacionesA(data.data);
                    
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));


        //VIAJERO 




        Controller.getMyPostViajera(idOtro)
            .then(data => {
                if (data.ok) {
                    setDatosPublicacionesV(data.data);
                    setEstrellitas(data.data[0].estrellitas);
                    console.log(datosPublicacionesV)
                   
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));



        //FOTOS

        Controller.getFotos(idOtro)
            .then(data => {
                if (data.ok) {
                    setDatosFotos(data.data);
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));

    }, [])

    //AHORA HAGO UN MAP PARA SACAR LOS DATOS QUE QUIERO
    // const Usuario=datosUsuario.map((el)=>{
    //     let edad = calculateAge(new Date(el.nacimiento));


    const PublicacionesA = datosPublicacionesA.map((el) => {
        return (<Divs className="col-12">
            <h3>{el.ciudad}</h3>
            {el.descripcion}
        </Divs>
        )
    });


    const PublicacionesV = datosPublicacionesV.map((el) => {
        return (<><Divs className="row" >
            <DivPublis>
                {el.descripcion}

              
            </DivPublis>

        </Divs>
            <hr />
        </>
        )
    });

    const Reviews = datosReviews.map((el) => {
        return (<Divs className="row">
            {el.descripcion}

            <hr />
        </Divs>
        )
    });

    const Comentarios = datosComentarios.map((el) => {
        let edadComent = calculateAge(new Date(el.nacimiento));
        return (
            <Divs className="col-12 p-4">
                <Row>
                    <Col className="col-4">
                        <Row>
                            <FotoPerfilNav2 imgSrc={API_FOTOS + el.foto} />
                        </Row>
                        <Row>
                            {el.nombre + ", " + edadComent}
                        </Row>
                    </Col>
                    <Col className="col-8">
                        {el.texto}
                    </Col>
                </Row>
            </Divs>
        )
    })

    const Fotos = datosFotos.map((el) => {
        let url = API_FOTOS + el.url;
        return (
            <Col className="col-lg-6 col-sm-12 mb-3">
                <DivFotoPrueba foto={url}>

                </DivFotoPrueba>
            </Col>
        )
    })



    let edad = calculateAge(new Date(nacimientoUsu));
    return (
        <>
         <NavBarPais pais={pais} id={id} foto={foto}/>
        <Fondo>
           
           

            <ContainerPrueba className="estContainer container" >
                <DescPagH1>{"Bienvenido al perfil de "+ nombreUsu}</DescPagH1>
                <hr />
                <Row >
                    <Col className="col-lg-4 col-sm-12 mt-5 ">
                        <Divs  >
                            <Row >
                                <Divs className="col">
                                    <Row style={{ justifyContent: "center" }}>
                                        <FotoPerfil imgSrc={API_FOTOS + fotoUsu} />
                                    </Row>
                                    <Row style={{ justifyContent: "center" }}> {nombreUsu + ", " + edad}</Row>
                                    <Row style={{ justifyContent: "center" }}>
                                        <StarsRating
                                            count={5}
                                            value={estrellitas}
                                            size={14}
                                            edit={false}
                                            color2={'#ffd700'} /></Row>

                                </Divs>
                            </Row>

                            <Row>
                                <DescPagH2>Comentarios <i class="fa fa-users" aria-hidden="true"></i></DescPagH2>
                                {Comentarios}



                            </Row>

                        </Divs>
                    </Col>
                    <Col className="col-lg-4 col-sm-12 mt-5">
                        <Divs  >
                            <Row>
                                <Col>
                                    <Row>
                                        <Col className="col-12 mt-3">
                                            <DescPagH2>Publicaciones</DescPagH2>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className="col-12 mt-2" style={{ height: "10%" }}>
                                            <DescPagH4>Viajera <i class="fa fa-plane" aria-hidden="true"></i></DescPagH4>
                                            {PublicacionesV}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className="col-12 mt-2">
                                            <DescPagH4>Anfitriona <i class="fa fa-home" aria-hidden="true"></i></DescPagH4>
                                            {PublicacionesA}
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>

                            <Row>
                                <DescPagH2>Experiencias</DescPagH2>
                                <Divs className="col-12">
                                    {Reviews}
                                </Divs>


                            </Row>

                        </Divs>
                    </Col>

                    <Col className="col-lg-4 col-sm-12 mt-5">
                        <Divs   >
                            <DescPagH2>Fotos  </DescPagH2>
                            <hr/>
                            <Row>

                                {Fotos}

                            </Row>

                        </Divs>
                    </Col>
                </Row>
            </ContainerPrueba>


            </Fondo>
        </>
    )
}

export default withCookies(PerfilOtro)