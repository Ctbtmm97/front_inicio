import React, { useState } from "react";
import { NavLink, Link } from "react-router-dom";
import styled from 'styled-components';
import ReviewComp from '../Elementos/Elementos.jsx';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Col,
  Nav,
  Row,
  Container
} from 'reactstrap';
import { withCookies } from "react-cookie";
import NavBarPais from '../Elementos/NavBarPais';

const FotoPerfilNav = styled.div`
border-radius:50%;
border:2px solid black;
width:50px;
height:50px;
display:inline-block;
background-size: cover;
background-position:center;
background-image:url("https://e7.pngegg.com/pngimages/43/926/png-clipart-computer-icons-avatar-user-avatar-heroes-woman.png")

`
  ;
const DescripcionPagina = styled.h1`
  font-family: 'Architects Daughter', cursive;
  padding:15px;
  `;


const DescPagPequeña = styled.h4`
  font-family: 'Architects Daughter', cursive;
  margin-top:15px;
  `;


const Reviews = (props) => {

  const pais = props.cookies.get("Compitrueno_paisElegido");
  const idPaisCookie = props.cookies.get("Compitrueno_idPaisElegido");
  const id = props.cookies.get("Compitrueno_idUsuario");
  const foto = props.cookies.get("Compitrueno_fotoUsuario");

  //DEL NAVBAR

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  //Cambiar al id que cojas con el get

  const url_reviews = `/inicio/${pais}/reviews`;

  return (
    <>
      <NavBarPais pais={pais} id={id} foto={foto} />


      <Container className=" estContainer ">
        <DescripcionPagina>
          Experiencias de nuestras Viajeras
          </DescripcionPagina>
        <hr />
        <Row>
          <Col>
          <DescPagPequeña><Link style={{ position: "absolute", right: "10px", color: "black", display: "inline-block", zIndex: "1" }} className="nav-link" to={url_reviews + "/publicacion"}> Añade tu publicación  + <i style={{ display: "inline-block" }} class="fa fa-address-card-o" aria-hidden="true"></i></Link> </DescPagPequeña>
          </Col>
        </Row>
        <Row>
          <Col>
            <ReviewComp idPais={idPaisCookie} />
            <hr />

          </Col>
        </Row>
      </Container>

    </>


  );
}

export default withCookies(Reviews)