import react from "react";
import { Container, Row, Col } from "reactstrap";
import styled from 'styled-components';

import StarsRating from "stars-rating";
import NavBarPerfil from "../Elementos/NavBarPerfil";
import { withCookies } from "react-cookie";

import Bea from "../Fotos/Bea.jpg";
import Edwin from "../Fotos/Edwin.jpg";
import Marc from "../Fotos/Marc.jpg";
import Dylan from "../Fotos/Dylan.jpg";

const FotoPerfil = styled.div`
border-radius:50%;
width:150px;
height:150px;
display:inline-block;
box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
margin:30px;
background-size: cover;
background-position:center;
background-image:url(${props => props.imgSrc})
`
    ;

const Estrellitas = styled.div`
display:inline-block;
`;
const BloqueContacto = styled.div`


border-radius:10px;
background-color:white;



`;
const PerfilUsuario = styled.div`
box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
border:2px solid #6F0CBB;
border-radius:20px;
margin-left:5px;
margin-right:5px;
padding:20px;
text-align:center;
position:relative;
height:100%;

`;

const Contacto = (props) => {
    const pais = props.pais;

    return (
        <>
            <NavBarPerfil pais={pais} />
            <Container fluid className=" estContainer ">
                <Row className="text-center ">
                        <BloqueContacto className="col-md-3 col-12 text-center  " >
                        <PerfilUsuario>
                            <Row >
                                
                                    <Col className="col-12" >
                                        <FotoPerfil imgSrc={Bea} />
                                        <h3>Beatriz Alonso</h3>
                                        <Estrellitas><StarsRating
                                            count={5}
                                            value={5}
                                            size={14}
                                            edit={false}
                                            color2={'#ffd700'} />
                                        </Estrellitas>
                                    </Col>
                       
                               </Row>
                                <Row >
                                    <Col className="col-12 m-2 text-center ">
                                        <br />

                                        <h4><i class="fa fa-envelope-o" aria-hidden="true"></i><br/> bea28.97@gmail.com</h4>
                                        <br />

                                        <h4><i class="fa fa-linkedin" aria-hidden="true"></i> BeatrizIbañez</h4>
                                        <br />

                                        <h4><i class="fa fa-phone" aria-hidden="true"></i> 619403064</h4>

                                    </Col>
                                </Row>
                                </PerfilUsuario>
                         </BloqueContacto>
                         <BloqueContacto className="col-md-3 col-12 text-center  " >
                        <PerfilUsuario>
                            <Row >
                                
                                    <Col className="col-12" >
                                        <FotoPerfil imgSrc={Edwin} />
                                        <h3>Edwin Leal</h3>
                                        <Estrellitas><StarsRating
                                            count={5}
                                            value={5}
                                            size={14}
                                            edit={false}
                                            color2={'#ffd700'} />
                                        </Estrellitas>
                                    </Col>
                       
                               </Row>
                                <Row >
                                    <Col className="col-12 m-2 text-center ">
                                        <br />

                                        <h4><i class="fa fa-envelope-o" aria-hidden="true"></i> edwinleal019@gmail.com</h4>
                                        <br />

                                        <h4><i class="fa fa-linkedin" aria-hidden="true"></i> Edwin-leal</h4>
                                        <br />

                                        <h4><i class="fa fa-phone" aria-hidden="true"></i>671620287</h4>

                                    </Col>
                                </Row>
                                </PerfilUsuario>
                         </BloqueContacto>
                         <BloqueContacto className="col-md-3 col-12 text-center  " >
                        <PerfilUsuario>
                            <Row >
                                
                                    <Col className="col-12" >
                                        <FotoPerfil imgSrc={Marc} />
                                        <h3>Marc Francisco</h3>
                                        <Estrellitas><StarsRating
                                            count={5}
                                            value={5}
                                            size={14}
                                            edit={false}
                                            color2={'#ffd700'} />
                                        </Estrellitas>
                                    </Col>
                       
                               </Row>
                                <Row >
                                    <Col className="col-12 m-2 text-center ">
                                        <br />

                                        <h4><i class="fa fa-envelope-o" aria-hidden="true"></i><br/> marc9223@gmail.com</h4>
                                        <br />

                                        <h4><i class="fa fa-linkedin" aria-hidden="true"></i> marcfg</h4>
                                        <br />

                                        <h4><i class="fa fa-phone" aria-hidden="true"></i> 620724211</h4>

                                    </Col>
                                </Row>
                                </PerfilUsuario>
                         </BloqueContacto>
                         <BloqueContacto className="col-md-3 col-12 text-center  " >
                        <PerfilUsuario>
                            <Row >
                                
                                    <Col className="col-12" >
                                        <FotoPerfil imgSrc={Dylan} />
                                        <h3>Dylan Aragón</h3>
                                        <Estrellitas><StarsRating
                                            count={5}
                                            value={5}
                                            size={14}
                                            edit={false}
                                            color2={'#ffd700'} />
                                        </Estrellitas>
                                    </Col>
                       
                               </Row>
                                <Row >
                                    <Col className="col-12 m-2 text-center ">
                                        <br />

                                        <h5><h4><i class="fa fa-envelope-o" aria-hidden="true"></i></h4> aragon.bernat.dylan@gmail.com</h5>
                                        <br />

                                        <h4><i class="fa fa-linkedin" aria-hidden="true"></i> dylan-aragon</h4>
                                        <br />

                                        <h4><i class="fa fa-phone" aria-hidden="true"></i> 663289858</h4>

                                    </Col>
                                </Row>
                                </PerfilUsuario>
                         </BloqueContacto>
                        
                       
                       
                </Row>
        </Container>
        </>

    )
}

export default withCookies(Contacto);