import React, { useState, useEffect } from "react";
import { NavLink, Link } from "react-router-dom";
import styled from 'styled-components';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  Button,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  Container,
  Row,
  Col
} from 'reactstrap';
import prueba from "../prueba.json";
import StarsRating from "stars-rating";
import { withCookies } from "react-cookie";
import NavBarPais from "../Elementos/NavBarPais";
import Controller from "../BienvenidaController";

import PerfectScrollbar from 'react-perfect-scrollbar'
import 'react-perfect-scrollbar/dist/css/styles.css';
const DescripcionPagina = styled.h1`
font-family: 'Architects Daughter', cursive;
padding:15px;
`;

const PerfilUsuario = styled.div`

border:2px solid #6F0CBB;
border-radius:20px;
background-color:#FFFAEB;
margin:5px;
padding:10px;
text-align:center;
position:relative;

`;

const ImagenPerfil = styled.div`

  width: 150px;
  height: 150px;
  border: 2px solid #6F0CBB;
  display:inline-block;
  border-radius: 200px;
  background-image: url( ${props => props.imgSrc});
  background-size:cover;
  background-position:center;
  
  
  `;

const DescPagPequeña = styled.h4`
font-family: 'Architects Daughter', cursive;
margin-top:15px;
`;

const Icono = styled.div`
  &:hover{
    cursor: pointer;
  }
`


const Anfitrion = (props) => {
  const pais = props.cookies.get("Compitrueno_paisElegido");
  const idPais = props.cookies.get("Compitrueno_idPaisElegido");
  const id = props.cookies.get("Compitrueno_idUsuario");
  const foto = props.cookies.get("Compitrueno_fotoUsuario");

  //DEL NAVBAR

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  //Cambiar al id que cojas con el get

  const url_anfitriones = `/inicio/${pais}/anfitriones`;

  const url_otroPerfil = `/inicio/perfil/`;

  const [dades, setDades] = useState([]);
  const [error, setError] = useState('');
  const API_FOTOS = 'http://localhost:3000/img/';

  useEffect(() => {

    Controller.getAnfitriones()
      .then(data => {
        if (data.ok) {
          console.log(data.data)
          setDades(data.data);
        } else {
          setError(data.err);
        }
      })
      .catch(err => setError(err.message));
  }, [])


  function calculateAge(birthday) {
    var ageDifMs = Date.now() - birthday;
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  function guardarAnfitriona(idAnfitriona) {
    let item = {
      usuario_id: id,
      anfitrion_id: idAnfitriona,
      pais_id: idPais
    }
    Controller.guardaAnfitriona(item);
  }

  const filas = dades
    .filter((el) => el.pais_id == idPais)
    .map((el) => {

      let edad = calculateAge(new Date(el.nacimiento));

      return (
        <Col className="col-lg-3 col-md-6 col-sm-12 p-1">
          <PerfilUsuario>
            <Icono>
              <i onClick={() => guardarAnfitriona(el.id)} class="fa fa-lg fa-user-plus" style={{ position: "absolute", top: "15px", right: "20px", color: "#6F0CBB" }} aria-hidden="true"></i>
            </Icono>
            <ImagenPerfil imgSrc={API_FOTOS + el.foto} />
            <h4> <Link style={{ color: '#6206AA' }} to={url_otroPerfil + el.id}>{el.nombre + ", " + edad}</Link></h4>
            <div style={{ display: "inline-block", verticalAlign: "bottom" }}>
              <h5>{el.ciudad}</h5>
              <StarsRating
                count={5}
                value={el.estrellitas}
                size={24}
                edit={false}
                color2={'#ffd700'} /></div>
            <PerfectScrollbar style={{ height: "100px" }}>
              <p>{el.descripcion}</p>
            </PerfectScrollbar>
          </PerfilUsuario >
        </Col>)
    });

  return (
    <>
      <NavBarPais pais={pais} id={id} foto={foto} />
      <Container  className=" estContainer " style={{ position: "relative" }}>
        <DescripcionPagina>Nuestras Anfitrionas</DescripcionPagina>
        <hr />
        <DescPagPequeña><Link style={{ position: "absolute", right: "10px", color: "black", display: "inline-block" }} className="nav-link" to={url_anfitriones + "/publicacion"}> Añade tu publicación  + <i style={{ display: "inline-block" }} class="fa fa-address-card-o" aria-hidden="true"></i></Link> </DescPagPequeña>
        <Row className="mt-5">{filas}</Row></Container>
    </>

  );
}

export default withCookies(Anfitrion);