import { v4 as uuid } from "uuid";
import axios from "axios";

const api_url = 'http://localhost:3000';

export default class Controller {

    //LOGIN

    static login = (data) => {
        return new Promise((ok, nok) => {

            fetch(api_url + '/usuarios/login', {
                method: 'POST',
                headers: new Headers({ 'Content-Type': 'application/json' }),
                body: JSON.stringify(data)
            })
                .then(resp => resp.json())
                .then(resp => {
                    if (resp.ok === false) {
                        nok(resp);
                    } else {
                        return resp.data;
                    }
                })
                .then(token => {
                    if (token) {
                        ok({ ok: true, token });
                    } else {
                        nok({ ok: false, err: "no token" })
                    }
                })
                .catch(err => nok({ ok: false, err }));
        })
    }

    // LOGOUT

    static logout = (token) => {
        return new Promise((ok, nok) => {
        fetch(api_url + "/usuarios/logout", {
          method: "DELETE",
          headers: new Headers({ "Content-Type": "application/json" }),
          body: JSON.stringify({ token }),
        })
          .then((res) => {
            ok({ok:true})
          })
          .catch((err) => {
              nok({ok: false, err})
          });
      });
    }


    //REGISTRARSE 

    static addUsuario = (item) => {
        const jsonUsuario = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonUsuario,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/usuarios", opcionesFetch)
            .then(resp => {
                console.log("nueva viajera:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));

    }

    //GET A UN USUARIO POR ID

    static getUsuarioById = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + '/usuarios/' + id)
                .then(data => data.json())
                .then(contacto => {
                    resuelve(contacto);
                })
                .catch(err => {
                    falla(err);
                });
        };

        return new Promise(promesa);
    }

    //MANDO NOMBRE Y PASSWORD Y ME DEVUELVE TRUE O FALSE

    static getVerificacion = (nombre, password) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/login")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);

    } 


    //REGISTRARSE 

    static nuevoUsuario = async (data) => {
        await axios.post(api_url + "/usuarios", data).then((res) => res.statusText);
    }



    static addUsuario = (item) => {
        const jsonUsuario = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonUsuario,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/usuarios", opcionesFetch)
            .then(resp => {
                console.log("nueva viajera:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));

    }

    //INICIO PAIS - RECIBE TODOS LOS PAISES
    static getPais = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/paises")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    };


    //INICIO PAIS - RECIBE ID Y PAIS - SI EL PAIS NO EXISTE LO CREA
    static getPaisNuevo = (pais) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/paises/" + pais)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                    console.log("getPais done " + data)
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //INICIO PAIS - RECIBE LAS EXPERIENCIAS DEL LOS PAISES
    static getReviewsPaises = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/experiencias")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //INICIO PAIS - RECIBE LAS EXPERIENCIAS DE UN PAIS ESPECIFICO
    static getReviewsPais = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/experiencias/pais/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //VIAJERAS - RECIBE TODAS LAS VIAJERAS
    static getViajeras = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/viajeros")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //PUBLICACION VIAJERA (SE NECESITA LA FECHA, PRESUPUESTO, DESCRIPCION, EL ID DEL PAIS Y ID DEL USUARIO)
    static addViajera = (item) => {
        const jsonViajera = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonViajera,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/viajeros", opcionesFetch)
            .then(resp => {
                console.log("nueva viajera:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));

    }

    //ANFITRIONAS - RECIBE TODAS LAS ANFITRIONAS
    static getAnfitriones = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/anfitriones")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //PUBLICACION ANFITRIONA (SE NECESITA LA DESCRIPCION, CIUDAD, EL ID DEL PAIS Y ID DEL USUARIO)
    static addAnfitriona = (item) => {
        const jsonAnfitriona = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonAnfitriona,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/anfitriones", opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));

    }

    //REVIEWS - RECIBE TODAS LAS EXPERIENCIAS
    static getExperiencias = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/experiencias")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //REVIEWS - RECIBE TODAS LAS EXPERIENCIAS DE UN PAIS ESPECIFICO
    static getExperienciasByPais = (id_pais) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/experiencias/pais/" + id_pais)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //REVIEWS - RECIBE TODAS LAS EXPERIENCIAS QUE EL USUARIO HA HECHO
    static getExperienciasById = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/experiencias/user/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //POST DE EXPERIENCIAS

    static addReview = (item) => {
        const jsonReview = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonReview,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/experiencias", opcionesFetch)
            .then(resp => {
                console.log("nueva review:", resp)
            })
            .catch(err => console.log("error al añadir tu publicación", err));

    }

    //---- PERFIL ----

    //GET MIS FOTOS(id): RECIBE LAS FOTOS QUE HA SUBIDO EL USUARIO

    static getFotos = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/fotos/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    static getFotosAll = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/fotos")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //RECIBE LOS COMENTARIOS QUE EL USUARIO HA RECIBIDO DEVUELVE NOMBRE, NACIMIENTO, ESTRELLITAS FOTO, TEXTO
    static getComentRecibidos = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/usuarios/coment/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }


    //GET DE LAS PUBLICACIONES QUE HAS PUBLICADO COMO VIAJERA 
    static getMyPostViajera = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/viajeros/usuario/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //GET DE LAS PUBLICACIONES QUE HAS PUBLICADO COMO ANFITRIONA 
    static getMyPostAnfitriona = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/anfitriones/usuario/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //---- FOTOS ----

    //POST NUEVA FOTO (SE NECESITA LA URL DE LA FOTO, EL ID DEL PAIS Y ID DEL USUARIO)
    static newFoto = (item) => {
        const jsonnewFoto = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonnewFoto,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/fotos", opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));

    }

    //BORRA UNA FOTO POR EL ID DE LA FOTO
    static borraFoto = (id) => {
        const opcionesFetch = {
            method: "DELETE",
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/fotos/" + id, opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }

    //---- PUBLICACIONES ----

    static EditarPublicacionViaje = (item) => {
        const jsonPubliViaje = JSON.stringify(item);
        const opcionesFetch = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: jsonPubliViaje
        }

        fetch(api_url + "/fotos/" + item.id, opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }


}

