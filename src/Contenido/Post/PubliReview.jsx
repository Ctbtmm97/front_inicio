import react, { useState } from "react";
import { Button, Form, FormGroup, Label, Col, Row, Input } from 'reactstrap';
import { Redirect } from "react-router-dom";
import { Link, } from "react-router-dom";
import styled from 'styled-components';
import { withCookies } from "react-cookie";
import Controller from "./BienvenidaController";

const ComponenteForm = styled.div`
text-align:center;
`;

const Titulo = styled.h1`
font-family: 'Architects Daughter', cursive;
`;

const Labels = styled.h4`
font-family: 'Architects Daughter', cursive;
`;

const FondoRegistro = styled.div`
background-color:white;
border: 5px solid black;
border-radius:10px;
text-align:center;
padding:20px;
margin-top: 100px;
box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
`;

const PubliReview = (props) => {

    const pais = props.cookies.get("Compitrueno_paisElegido");
    const nombre = props.cookies.get("Compitrueno_nombreUsuario");
    const apellidos = props.cookies.get("Compitrueno_apellidosUsuario");
    const id = props.cookies.get("Compitrueno_idUsuario");
    const foto = props.cookies.get("Compitrueno_fotoUsuario");
    const idPais = props.cookies.get("Compitrueno_idPaisElegido");

    const url_reviews = `/inicio/${pais}/reviews`;

    const [texto, setTexto] = useState("");
    const [fechaViaje,setFechaViaje]=useState("");
    const [volver, setVolver] = useState(false);

    const Publicar = () => {
        const Publicacion = {
            usuario_id: id,
            pais_id: idPais,
            descripcion: texto,
            fecha: fechaViaje
        };
        Controller.addReview(Publicacion);
        setVolver(true);
    }

    if (volver) {
        return (
            <Redirect to={url_reviews} />
        )
    }

    return (

        <>
            <FondoRegistro className="container">
                <div><Titulo>Publicando Experiencias!!</Titulo></div>
                <Form>
                    <Row>
                        <ComponenteForm className="col-lg-6">
                            <Label for="idnombreapellidos"><Labels>Nombre y Apellidos</Labels></Label>
                            <Row>
                                <Col className="col-6">
                                    <Input type="text" name="nombre" id="idnombreapellidos" value={nombre} disabled="disabled" />
                                </Col>
                                <Col className="col-6">
                                    <Input type="text" name="apellidos" id="idnombreapellidos" value={apellidos} disabled="disabled" />
                                </Col>
                            </Row>
                        </ComponenteForm>
                    </Row>
                    <Row>
                    <ComponenteForm className="col-lg-6">
                            <FormGroup>
                                <Label for="fecha"><Labels>Fecha del viaje</Labels></Label>
                                <Input className="Input," type="date" name="iniviaje" value={fechaViaje} id="fecha" onChange={e=>setFechaViaje(e.target.value)}/>
                            </FormGroup>
                        </ComponenteForm>
                    </Row>
                    <Row>
                        <ComponenteForm className="col-lg-12">
                            <FormGroup>
                                <Label for="exampleText"> <Labels>Descripcion</Labels></Label>
                                <Input className="Input," type="textarea" name={texto} id="exampleText" onChange={e => setTexto(e.target.value)} placeholder="Cuéntanos tu experiencia en este país." />
                            </FormGroup>
                        </ComponenteForm>
                    </Row>
                    <Button className="m-3" onClick={Publicar}> Enviar Publicación</Button>
                    <Link style={{color:"white"}} to={url_reviews}><Button>Volver a Experiencias </Button></Link>
                </Form>
            </FondoRegistro>
        </>
    )
}

export default withCookies(PubliReview);