import react,{useState,useEffect} from "react";
import {
    Button, Form, FormGroup, Label, Col, Row,
    Input,
   
} from 'reactstrap';
import { Redirect } from "react-router-dom";

import {  Link, } from "react-router-dom";
import styled from 'styled-components';
import { withCookies } from "react-cookie";
import Controller from "./BienvenidaController";


const ComponenteForm = styled.div`

text-align:center;

`;

const Titulo=styled.h1`
font-family: 'Architects Daughter', cursive;
`;

const Labels = styled.h4`
font-family: 'Architects Daughter', cursive;
`;

const FondoRegistro = styled.div`

background-color:white;
border: 5px solid black;
border-radius:10px;
text-align:center;
padding:20px;
margin-top: 100px;
box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
`;

const PubliViajera =(props) => {


    const nombre = props.cookies.get("Compitrueno_nombreUsuario");
    const apellidos = props.cookies.get("Compitrueno_apellidosUsuario");
    const id = props.cookies.get("Compitrueno_idUsuario");
    
    

      const url_perfil = `/perfil/${id}`;

    const [idPais,setIdPais]=useState("");
    const [volver,setVolver]=useState(false);
    const[filename,setFilename]=useState(false)
    const [listaPaises,setListaPaises]=useState([]);
    
    useEffect(()=>{
    Controller.getPais()
    .then((data)=>setListaPaises(data))
    .then(()=>console.log(listaPaises));
    },[])

  const Publicar=()=>{ 
    const data = new FormData();
        data.append("file",filename);
        data.append("pais_id",idPais);
        data.append("usuario_id",id);
       
    
    Controller.nuevaFoto(data)
    .then(()=> setVolver(true));

}

    if(volver){
        return(
        <Redirect to={url_perfil}/>
        )
    }

    return (

        <>
            <FondoRegistro className="container">
                <div><Titulo>Publica tu viaje</Titulo></div>
                <Form>
                    <Row>
                        <ComponenteForm className="col-lg-6">
                            <FormGroup>
                                <Label for="idnombre"><Labels>Nombre</Labels></Label>
                                <Input className="Input," type="text" name="nombre" id="idnombre" value={nombre} disabled="disabled" />
                            </FormGroup>
                        </ComponenteForm>
                        <ComponenteForm className="col-lg-6">
                            <FormGroup>
                                <Label for="idapellidos"><Labels>Apellidos</Labels></Label>
                                <Input className="Input," type="text" name="apellidos" id="idapellidos" value={apellidos} disabled="disabled" />
                            </FormGroup>
                        </ComponenteForm>
                    </Row>
                    <Row>
                        
                   
                    <Button className="m-3" onClick={Publicar}> Enviar Publicación</Button>
                    <Link style={{color:"white"}} to={url_perfil}><Button>Volver a Perfil </Button></Link>
               
               </Row> </Form>
            </FondoRegistro>
        </>
    )
}

export default withCookies(PubliViajera);