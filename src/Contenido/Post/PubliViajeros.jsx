import react,{useState} from "react";
import {
    Button, Form, FormGroup, Label, Col, Row,
    Input,
   
} from 'reactstrap';
import { Redirect } from "react-router-dom";

import {  Link, } from "react-router-dom";
import styled from 'styled-components';
import { withCookies } from "react-cookie";
import Controller from "./BienvenidaController";


const ComponenteForm = styled.div`

text-align:center;

`;

const Titulo=styled.h1`
font-family: 'Architects Daughter', cursive;
`;

const Labels = styled.h4`
font-family: 'Architects Daughter', cursive;
`;

const FondoRegistro = styled.div`

background-color:white;
border: 5px solid black;
border-radius:10px;
text-align:center;
padding:20px;
margin-top: 100px;
box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
`;

const PubliViajera =(props) => {

    const pais = props.cookies.get("Compitrueno_paisElegido");
    const nombre = props.cookies.get("Compitrueno_nombreUsuario");
    const apellidos = props.cookies.get("Compitrueno_apellidosUsuario");
    const email = props.cookies.get("Compitrueno_emailUsuario");
    const contrasenya = props.cookies.get("Compitrueno_contrasenyaUsuario");
    const valoracion = props.cookies.get("Compitrueno_valoracionUsuario");
    const fecha = props.cookies.get("Compitrueno_fechaUsuario");
    const id = props.cookies.get("Compitrueno_idUsuario");
    const foto = props.cookies.get("Compitrueno_fotoUsuario");
    const idPais = props.cookies.get("Compitrueno_idPaisElegido");

    const url_viajeros = `/inicio/${pais}/viajeros`;

    const [texto,setTexto]=useState("");
    const [fechaViaje,setFechaViaje]=useState("");
    const [dinero,setDinero]=useState("");
    const [volver,setVolver]=useState(false);

    const Publicar=()=>{
        const Publicacion={
            usuario_id:id,
           
            pais_id:idPais,
          
            descripcion: texto,
            fecha: fechaViaje,
            presupuesto: dinero
        };
        
        Controller.addViajera(Publicacion);
         setVolver(true);
    }

    if(volver){
        return(
        <Redirect to={url_viajeros}/>
        )
    }

    return (

        <>
            <FondoRegistro className="container">
                <div><Titulo>Publica tu viaje</Titulo></div>
                <Form>
                    <Row>
                        <ComponenteForm className="col-lg-6">
                            <FormGroup>
                                <Label for="idnombre"><Labels>Nombre</Labels></Label>
                                <Input className="Input," type="text" name="nombre" id="idnombre" value={nombre} disabled="disabled" />
                            </FormGroup>
                        </ComponenteForm>
                        <ComponenteForm className="col-lg-6">
                            <FormGroup>
                                <Label for="idapellidos"><Labels>Apellidos</Labels></Label>
                                <Input className="Input," type="text" name="apellidos" id="idapellidos" value={apellidos} disabled="disabled" />
                            </FormGroup>
                        </ComponenteForm>
                    </Row>
                    <Row>
                        <ComponenteForm className="col-lg-6">
                            <FormGroup>
                                <Label for="fecha"><Labels>Inicio del viaje</Labels></Label>
                                <Input className="Input," type="date" name="iniviaje" value={fechaViaje} id="fecha" onChange={e=>setFechaViaje(e.target.value)}/>
                            </FormGroup>
                        </ComponenteForm>
                        <ComponenteForm>
                            <Col className="col-lg-12">
                                <Label for="ididentificacion"><Labels>Presupuesto</Labels></Label>
                                <Input className="Input," type="select" name="nombre" id="ididentificacion" onChange={(e)=>setDinero(e.target.value)}>
                                    <option value={1}>Opción presupuesto bajo</option>
                                    <option value={2}>Opción presupuesto medio</option>
                                    <option value={3}>Opción presupuesto alto</option>
                                </Input></Col>
                        </ComponenteForm>
                    </Row>
                    <Row>
                        <ComponenteForm className="col-lg-12">
                            <FormGroup>
                                <Label for="exampleText"> <Labels>Describe tu Viaje</Labels></Label>
                                <Input className="Input," type="textarea" name={texto} id="exampleText" onChange={e=>setTexto(e.target.value)} placeholder="Escribe información que pueda interesar a otras viajeras ¿Viajas con más gente?¿Qué ciudades te gustaría recorrer? ¿Algún plan que no pueda faltar en tu viaje? ¿Cuánto tiempo vas a viajar?" />
                            </FormGroup>
                        </ComponenteForm>
                    </Row>
                    <Button className="m-3" onClick={Publicar}> Enviar Publicación</Button>
                    <Link style={{color:"white"}} to={url_viajeros}><Button>Volver a Viajeras </Button></Link>
                </Form>
            </FondoRegistro>
        </>
    )
}

export default withCookies(PubliViajera);