import React, { useState, useEffect } from "react";
import { NavLink, Link, } from "react-router-dom";
import styled from 'styled-components';
import StarsRating from "stars-rating";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Container,
  Col,
  Row,

  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  Button
} from 'reactstrap';

import PerfectScrollbar from 'react-perfect-scrollbar'
import 'react-perfect-scrollbar/dist/css/styles.css';

import prueba from "../prueba.json";
import { withCookies } from "react-cookie";
import NavBarPais from "../Elementos/NavBarPais.jsx";
import Controller from "../BienvenidaController";


const FotoPerfilNav = styled.div`
border-radius:50%;
border:2px solid black;
width:50px;
height:50px;
display:inline-block;
background-size: cover;
background-position:center;
background-image:url("https://e7.pngegg.com/pngimages/43/926/png-clipart-computer-icons-avatar-user-avatar-heroes-woman.png")

`
  ;
// F8EBFF
const PerfilUsuario = styled.div`

border:2px solid #6F0CBB;
border-radius:20px;
background-color:#FFFAEB;
margin:5px;
padding:10px;
text-align:center;
position:relative;

`;
const ImagenPerfil = styled.div`

width: 150px;
height: 150px;
border: 2px solid #6F0CBB;
display:inline-block;
border-radius: 200px;
background-image: url( ${props => props.imgSrc});
background-size:cover;
background-position:center;


`;

const FechaViajero = styled.div`

margin-left:5px;
margin-right:10px;


`;


const DescripcionPagina = styled.h1`
font-family: 'Architects Daughter', cursive;
margin-top:15px;
`;
const DescPagPequeña = styled.h4`
font-family: 'Architects Daughter', cursive;
margin-top:15px;
`;
const EurosPresupuesto = styled.div`

display:inline-block;
padding:7px;

`;

const FechaViaje = styled.span`
font-size:20px;
padding-right:10px;
`;

const Icono = styled.div`
  &:hover{
    cursor: pointer;
  }
`


const Viajeros = (props) => {

  const pais = props.cookies.get("Compitrueno_paisElegido");
  const idPais = props.cookies.get("Compitrueno_idPaisElegido");
  const id = props.cookies.get("Compitrueno_idUsuario");
  const foto = props.cookies.get("Compitrueno_fotoUsuario");

  //DEL NAVBAR


  //Cambiar al id que cojas con el ge
  const url_viajeros = `/inicio/${pais}/viajeros`;

  const url_otroPerfil = `/inicio/perfil/`;

  const [dades, setDades] = useState([]);
  const [error, setError] = useState('');

  const API_FOTOS = 'http://localhost:3000/img/';

  useEffect(() => {

    Controller.getViajeras()
      .then(data => {
        if (data.ok) {
          console.log(data.data)
          setDades(data.data);
        } else {
          setError(data.err);
        }
      })
      .catch(err => setError(err.message));
  }, [])

  const Presupuesto = (x) => {
    if (x == 1) {
      return (<i class="fa fa-eur" style={{ color: "green" }} aria-hidden="true" />);
    }
    else if (x == 2) {
      return (<><i class="fa fa-eur" aria-hidden="true" /> <i class="fa fa-eur" aria-hidden="true" /></>);
    }
    else return (<><i class="fa fa-eur" style={{ color: "#B61818", margin: "2px" }} aria-hidden="true" /><i class="fa fa-eur" style={{ color: "#B61818", margin: "2px" }} aria-hidden="true" /><i class="fa fa-eur" style={{ color: "#B61818", margin: "2px" }} aria-hidden="true" /></>);

  }

  function calculateAge(birthday) {
    var ageDifMs = Date.now() - birthday;
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  function guardarViajera(idViajera) {
    let item = {
      usuario_id: id*1,
      viajero_id: idViajera*1,
      pais_id: idPais*1
    }
    Controller.guardaViajera(item);
  }

  const DatosViajeras = dades
    .filter((el) => el.pais_id == idPais)
    .map((el) => {
      let edad = calculateAge(new Date(el.nacimiento));
      return (
        <Col className="col-lg-3 col-md-6 col-sm-12 p-1" >
          <PerfilUsuario >
            <Icono>
              <i onClick={() => guardarViajera(el.idv)} class="fa fa-lg fa-user-plus" style={{ position: "absolute", top: "15px", right: "20px", color: "#6F0CBB" }} aria-hidden="true"></i>
            </Icono>
            <ImagenPerfil imgSrc={API_FOTOS + el.foto} />
            <h4> <Link style={{ color: '#6206AA' }} to={url_otroPerfil + el.id}>{el.nombre + ", " + edad}</Link></h4>
            <div style={{ display: "inline-block", verticalAlign: "bottom" }}>
              <StarsRating
                count={5}
                value={el.estrellitas}
                size={24}
                edit={false}
                color2={'#ffd700'} /></div>

            <FechaViajero>
              <FechaViaje style={{ display: "inline-block" }}><strong>
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>{" " + el.fecha}</strong>
              </FechaViaje>
            </FechaViajero>
            <EurosPresupuesto>{Presupuesto(el.presupuesto)}</EurosPresupuesto>
            <PerfectScrollbar style={{ height: "100px" }}>
              <p>{el.descripcion}</p>
            </PerfectScrollbar>
          </PerfilUsuario >
        </Col>
      )
    });

  return (
    <>
      <NavBarPais pais={pais} id={id} foto={foto} />

      <Container  className=" estContainer " style={{ position: "relative" }} >
        <DescripcionPagina>Nuestras Viajeras </DescripcionPagina>
        <hr />


        <DescPagPequeña><Link style={{ position: "absolute", right: "10px", color: "black", display: "inline-block" }} className="nav-link" to={url_viajeros + "/publicacion"}> Añade tu publicación  + <i style={{ display: "inline-block" }} class="fa fa-address-card-o" aria-hidden="true"></i></Link> </DescPagPequeña>


        <Row className="mt-5">{DatosViajeras}</Row></Container>


    </>

  );
}

export default withCookies(Viajeros);