import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import styled from 'styled-components';
import {
  Collapse,
  Navbar,
  NavbarToggler,

  Nav,
  Col,
  Row,


  Container
} from 'reactstrap';
import PerfectScrollbar from 'react-perfect-scrollbar'
import 'react-perfect-scrollbar/dist/css/styles.css';

import Carrusel from '../Elementos/CarruselFotos.jsx';
import ReviewComp from '../Elementos/Elementos.jsx';
import { withCookies } from "react-cookie";
import NavBarPais from '../Elementos/NavBarPais';

const DescripcionPagina = styled.h1`
font-family: 'Architects Daughter', cursive;
padding:15px;
`;



const PaisInicio = (props) => {

  const pais = props.cookies.get("Compitrueno_paisElegido");
  const id = props.cookies.get("Compitrueno_idUsuario");
  const foto = props.cookies.get("Compitrueno_fotoUsuario");


  const idPaisCookie = props.cookies.get("Compitrueno_idPaisElegido");

  //DEL NAVBAR


   //Cambiar al id que cojas con el get




  return (
    <>
      <NavBarPais pais={pais} id={id} foto={foto}/>
     
      <Container className=" estContainer "> 
        <DescripcionPagina>Fotos de nuestras Viajeras</DescripcionPagina>
      <hr/>
        <Row>
          <Col>
            <Carrusel idPais={idPaisCookie} />
          </Col>
        </Row>
        <Row>
          <Col>
          <DescripcionPagina>
            Experiencias de nuestras Viajeras
          </DescripcionPagina>
          <hr/>
            <PerfectScrollbar style={{ height: "500px" }}>
              <ReviewComp idPais={idPaisCookie} />
            </PerfectScrollbar>
          </Col>
        </Row>
      </Container>
    </>

  );
}

export default withCookies(PaisInicio)