import react from "react";
import styled from 'styled-components';
import FotoNav from '../Fotos/NavBar.jpg';
import { NavLink } from "react-router-dom";

const FooterStyle = styled.div`
display:inline-block;

background-size: contain;
background-position:center;
background-image: linear-gradient(rgba(255,255,255,.5), rgba(255,255,255,.5)),url(${props => props.imgSrc});
    
    border-top: "1px solid #E7E7E7";
    text-align: center;
    padding: 20px;
    position: fixed;
  
border:2px solid black;
    left: 0;
    bottom: 0;
    height: 75px;
    width: 100%;`

    ;

const Labels = styled.h4`
font-family: 'Architects Daughter', cursive;
color:black;
font-weight:400;
`;

const Footer = ()=>{
    
        return (
           <>
           <FooterStyle imgSrc={FotoNav}>
           <NavLink style={{color:"black"}}to="/contacto"> <Labels>¿Tienes algún problema, sugerencia o comentario de la página? Contacta con nosotros </Labels> </NavLink>
           </FooterStyle>
           </>
        );
    };


export default Footer;