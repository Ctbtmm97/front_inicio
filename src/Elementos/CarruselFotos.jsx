import React, { useState, useEffect } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';

import styled from "styled-components";


import Controller from "../BienvenidaController";

const FotosCarrusel = styled.div`
width:100%;
height:500px;
background-size: cover;
background-position:center;
display:inline-block;
background-image: url( ${props => props.imgSrc});

`;

// const items = prueba.map((el)=>( 
//   {
//     src: el.url,
//     altText: el.nombre,
//     caption: el.name
//   }));

//Habría que eliminar este item y coger directamente en el props el foto.url foto.pais para el nombre slide 1 (Si queremos el nombre del id.usuario para el subtitulo)

const Carrusel = (props) => {

  const [dades, setDades] = useState([]);
  const [error, setError] = useState();

  useEffect(() => {

    Controller.getFotosAll()
      .then(data => {
        if (data.length) {
          let items = data.filter((el) => el.pais_id == props.idPais);
          
          if (items.length === 0) {
            items = [{ url: "croquetas.jpeg" }];
          }
          setDades(items);
        } else {
          setError(data.err);
        }
      })
      .catch(err => setError(err.message));
  }, [])

  const API_FOTOS = 'http://localhost:3000/img/';
  let items = dades.map((el) => (
    API_FOTOS + el.url
  ))

  console.log(items);

  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item}
      >
        <FotosCarrusel imgSrc={item} />

        <CarouselCaption />
      </CarouselItem>
    );
  });

  return (
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
    >
      <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    </Carousel>
  );
}

export default Carrusel;