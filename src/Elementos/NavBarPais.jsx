import react, { useState,useEffect } from "react";
import { NavLink, Link } from "react-router-dom";

import {
    Collapse,
    Navbar,
    Nav,
    NavbarToggler,
    Button,
    Container,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,

    NavbarText,
    Col,
    Row
} from 'reactstrap';

import StarsRating from "stars-rating";

import styled from "styled-components";

import RayoFondo from '../Fotos/RayoBienInicio.png';
import FotoNav from '../Fotos/NavBar.jpg';
import prueba from "../prueba.json";

import Controller from "../BienvenidaController";
import {withCookies} from "react-cookie";
import "../Todo.css";


const FotoPerfilNav = styled.div`
border-radius:50%;
border:2px solid black;
width:50px;
height:50px;
display:inline-block;

background-size: cover;
background-position:center;
background-image:url(${props => props.imgSrc});

`
  ;
  const Labels = styled.h4`
  font-family: 'Architects Daughter', cursive;
  color:white;
  `;
 
const NavBarPais=(props)=>{
    
    const pais = props.pais;
    
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    const [error, setError] = useState('');

    const [datosFotos, setDatosFotos] = useState([]);
    const id = props.id;
    const foto=props.foto;



    const url_viajeros=`/inicio/${pais}/viajeros`;
    const url_anfitriones=`/inicio/${pais}/anfitriones`;
    const url_reviews=`/inicio/${pais}/reviews`;
    const url_perfil=`/perfil/${id}`;
    const url_configuracion=`/configuracion/${id}`;
    const url_guardados=`/guardados/${id}`;
    const url_paisInicio=`/inicio/${pais}`;
    const url_otroPerfil=`/inicio/perfil/`;


        //FOTOS
useEffect(()=>{
        Controller.getFotos(id)
            .then(data => {
                if (data.ok) {
                    setDatosFotos(data.data);
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));

    }, [])

    const API_FOTOS = 'http://localhost:3000/img/';
    


    return(
        <Navbar  className="estNavbar" light expand="md">
      
        <NavLink exact className="nav-link" to="/inicio">
         <img src={RayoFondo} width="60px" height="60px"/>
          </NavLink>
      
      
          <NavbarText className="m-auto"> <Labels> <Link style={{color:"white"}} to={url_paisInicio}> { pais}</Link> </Labels> </NavbarText>
    <NavbarToggler  onClick={toggle} />
   
    <Collapse isOpen={isOpen} navbar>
  

  
  <NavLink exact className="nav-link ml-auto" to={url_viajeros}>
         <Labels> Viajeros </Labels>
          </NavLink>
  
  
  <NavLink exact className="nav-link ml-auto" to={url_anfitriones}>
        <Labels>  Anfitriones</Labels>
          </NavLink>
  
  
  <NavLink exact className="nav-link  ml-auto" to={url_reviews}>
         <Labels> Experiencias</Labels>
          </NavLink>
  
  
      <Nav className="ml-auto" navbar>
       
       
        <UncontrolledDropdown nav right>
          <DropdownToggle nav caret right>
          <FotoPerfilNav imgSrc={API_FOTOS + foto} />
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
            <NavLink exact className="nav-link" to={url_perfil}>
          Perfil
          </NavLink>
              
            </DropdownItem>
            <DropdownItem>
            <NavLink exact className="nav-link" to={url_configuracion}>
          Configuración
          </NavLink>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem>
            <NavLink exact className="nav-link" to={url_guardados}>
          Guardado
          </NavLink>
            </DropdownItem>
         
          <DropdownItem>
            <NavLink exact className="nav-link" to="/iniciar">
          Cerrar Sesión
          </NavLink>
            </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
      </Nav>
     
    </Collapse>
  </Navbar>

    );}

    export default NavBarPais;