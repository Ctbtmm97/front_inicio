import react, { useState,useEffect } from "react";
import { NavLink, Link } from "react-router-dom";

import {
    Collapse,
    Navbar,
    Nav,
    NavbarToggler,
    NavbarText,
    Button,
    Container,
    Col,
    Row
} from 'reactstrap';
import styled from 'styled-components';
import RayoFondo from '../Fotos/RayoBienInicio.png';

import '../Todo.css';
const Labels = styled.h4`
font-family: 'Architects Daughter', cursive;
color:white;
`;




const NavBarPerfil=(props)=>{
    
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    const pais = props.pais;

    const id=1;

    const url_perfil = `/perfil/${id}`;
    const url_configuracion = `/configuracion/${id}`;
    const url_guardados = `/guardados/${id}`;
    const url_paisInicio=`/inicio/${pais}`;


    return(
        <Navbar className="estNavbar"  light expand="md">
                <NavLink exact className="nav-link" to="/inicio">
                <img src={RayoFondo} width="60px" height="60px"/>
                </NavLink>
                <NavbarText className="m-auto">  <Labels><Link style={{color:"white"}} to={url_paisInicio}> { pais}</Link> </Labels> </NavbarText>

                <NavbarToggler onClick={toggle} />

                <Collapse isOpen={isOpen} navbar>
                    <NavLink exact className="nav-link ml-auto" to={url_perfil}>
                        <Labels>Perfil</Labels>
                </NavLink>

                    <NavLink exact className="nav-link ml-auto" to={url_configuracion}>
                       <Labels>Configuración</Labels> 
                </NavLink>
                    <NavLink exact className="nav-link ml-auto" to={url_guardados}>
                        <Labels>Guardado</Labels>
                </NavLink>
                    <NavLink exact className="nav-link ml-auto" to="/iniciar">
                       <Labels> Cerrar Sesión </Labels>
                </NavLink>
                </Collapse>
            </Navbar>

    );
}

export default NavBarPerfil;