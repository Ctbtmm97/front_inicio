import React from "react";
import Datamap from "react-datamaps";

class Page extends React.Component {
  constructor(props) {
    super(props);
  }
  addClickHandlers = ref => {
    if (ref && ref.map) {
      ref.map.svg.selectAll(".datamaps-bubble").on("click", bubble => {
        if (this.props.onClick) {
          this.props.onClick(bubble);
        }
      });
    }
  };
  render() {
    const { style } = this.props;
    return (
      <div style={style}>
        <Datamap
          ref={this.addClickHandlers}
          scope="world"
          responsive
          geographyConfig={{
            popupOnHover:false,
            highlightOnHover:true
          }}
          
       
         
        />

      </div>
    );
  }
}

export default Page;
