import react, { useState, useEffect } from "react";
import { NavLink, Link } from "react-router-dom";

import {
    Collapse,
    Navbar,
    Nav,
    NavbarToggler,
    Button,
    Container,
    Col,
    Row
} from 'reactstrap';

import StarsRating from "stars-rating";

import styled from "styled-components";

import prueba from "../prueba.json";

import Controller from "../BienvenidaController";
import { withCookies } from "react-cookie";


const FotoPerfilNav2 = styled.div`
border-radius:50%;

width:50px;
height:50px;
display:inline-block;
margin:5px;
margin-top:20px;
background-size: cover;
background-position:center;
background-image:url(${props => props.imgSrc});
` ;


const ReviewComp = (props) => {

    const [dades, setDades] = useState([]);
    const [error, setError] = useState('');
    const API_FOTOS = 'http://localhost:3000/img/';
   

    useEffect(() => {

        Controller.getReviewsPais(props.idPais)
            .then(data => {
                if (data.ok) {
                    console.log("Holita estas en el data.ok")
                    console.log("Esto es el data" + data)

                    setDades(data.data);
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));
    }, [props.idPais])

    function calculateAge(birthday) {
        var ageDifMs = Date.now() - birthday;
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    const Reviews = dades.map((el) => {

        let edad = calculateAge(new Date(el.nacimiento));

        return (
            <Row style={{ borderBottom: "1px solid black", padding: "10px" }}>

                <Col className="col-lg-2 col-12 text-center" style={{ display: "flex", justifyContent: "center" }}>
                    <Row>
                        <Col>
                            <FotoPerfilNav2 imgSrc={API_FOTOS + el.foto } />
                            <p >{el.nombre + ", " + edad} </p>
                            <StarsRating
                                count={5}
                                value={el.calificacion}
                                size={14}
                                edit={false}
                                color2={'#ffd700'} />
                        </Col>
                    </Row>

                </Col>
                <Col className="col-lg-10  col-12 p-5" >
                    <span style={{ display: "flex", verticalAlign: "center", justifyContent: "center" }}>
                        {el.descripcion}
                    </span>
                </Col>
                <hr />
            </Row>
        )
    });

    return (<>
        
        <Container >

            {Reviews}

        </Container>

    </>
    );

}




export default withCookies(ReviewComp)