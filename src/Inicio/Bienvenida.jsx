import react from 'react';
import {Button,Container,Col,Row} from "reactstrap";
import {  NavLink, Link } from "react-router-dom";
import styled from 'styled-components';
import RayoFondo from '../Fotos/RayoBienInicio.png';


const Titulo=styled.h1`
font-family: 'Architects Daughter', cursive;
`;

const DescripcionPagina=styled.h5`
font-family: 'Architects Daughter', cursive;
`;

const Fondo=styled.div`
background-color:white;
background-image: url(${RayoFondo});
display:inline-block;
background-position:center;

width:100%;
height:100vh;
padding-top:200px;


`;
const FondoRegistro = styled.div`

background-color:white;
border: 5px solid black;
border-radius:10px;
text-align:center;
padding:20px;
margin-top: 20px;

box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
`;

export default()=>{
    return(
        <>
        <Fondo>
        <FondoRegistro className=" container text-center pt-5"  ><Row ><Col>
        <Titulo>Bienvenido a CompiTrueno</Titulo>
        <DescripcionPagina>CompiTrueno es una  red social inspirada en todas aquellas mujeres 
            que quieren viajar alrededor del mundo pero buscan una compañera de viaje con quien compartir aventuras. 
            En CompiTrueno no solo podrás encontrar a muchas mujeres en tu misma situación
             , sino que podrás encontrar anfitriones de esos paises. 
             Que mejor manera de sumergirte en la cultura de un pais que con una cerveza!   
             
                </DescripcionPagina>
                <Link to="/iniciar"> <Button className="m-4" outline color="secondary" >Iniciar Sesión</Button></Link>
        <Link to="/registrarse"><Button className="m-4" outline color="secondary"> Registrarse</Button></Link>
        </Col></Row></FondoRegistro>
        </Fondo>
        </>
    );
}