import react, { useState } from "react";
import { NavLink, Link, Redirect } from "react-router-dom";
import { Container, Button, Form, Row, Col, Label, Input, FormGroup, FormText } from "reactstrap";
import styled from 'styled-components';
import RayoFondo from '../Fotos/RayoBienInicio.png';
import Controller from "../BienvenidaController";

const FondoRegistro = styled.div`

background-color:white;
border: 5px solid black;
border-radius:10px;
text-align:center;

margin-top: 100px;
box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
padding-bottom:200px;
margin-bottom:200px;



`;
const ComponenteForm = styled.div`

text-align:center;

`;
const Labels = styled.h4`
font-family: 'Architects Daughter', cursive;
`;
const FondoRayos = styled.div`
background-color:white;
background-image: url(${RayoFondo});
display:inline-block;
background-position:center;
width:100%;
height:100vh;
padding-top:10px;
padding-bottom:200px;


`;
const FotoPerfil = styled.div`
border-radius:50%;
width:150px;
height:150px;
box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
display:inline-block;
margin:30px;
background-size: cover;
background-position:center;
background-image:url("https://e7.pngegg.com/pngimages/43/926/png-clipart-computer-icons-avatar-user-avatar-heroes-woman.png")

`
    ;

const LabelsError = styled.p`
font-family: 'Architects Daughter', cursive;
color: red;
font-size: 1.5em;
`;

export default (props) => {
    const [nombre, setNombre] = useState("");
    const [apellidos, setApellidos] = useState("");
    const [nacimiento, setNacimiento] = useState("");
    const [correo, setCorreo] = useState("");
    const [contrasenya, setContrasenya] = useState("");
    const [filename, setFilename] = useState(false);
    const [errorInputVacio, setErrorInputVacio] = useState(false);

    const [volver, setVolver] = useState(false);



    const Registrarse = () => {
        if (nombre === ""|| apellidos === ""|| nacimiento === ""|| correo === ""|| contrasenya === "") {

            setErrorInputVacio(true);
        } else {
            const data = new FormData();
            data.append("nombre", nombre);
            data.append("apellidos", apellidos);
            data.append("nacimiento", nacimiento);
            data.append("correo", correo);
            data.append("contrasenya", contrasenya);
            data.append("file", filename);


            Controller.nuevoUsuario(data)
                .then(() => setVolver(true))

                .catch((err) => {
                    console.log("err login: ", err);
                });
        }


    }
    if (volver) {
        return (
            <Redirect to="/iniciar" />
        )
    }
    return (
        <>

            <FondoRayos>

                <FondoRegistro className="container">
                    <Form>
                        <Row className="row mt-2 mb-3 text-center">
                            <Col className="col-12 text-center">
                                <Row >
                                    <Col className="text-center col-12">
                                        <FotoPerfil />
                                    </Col>
                                </Row>
                                <Row className="offset-3 text-center col-12 mr-5 ml-5">
                                    <Col>
                                        <Input style={{ display: "inline-block", width: "30%" }} type="file" name="file" id="FotoPerfil" onChange={(ev) => setFilename(ev.target.files[0])} />
                                        <FormText color="muted">
                                            Procura que tu foto de perfil sea clara e individual donde todo el mundo pueda reconocerte correctamente. Gracias por contribuir al bienestar de la página.
                                            {errorInputVacio && <LabelsError>Alguno de los campos no se han rellenado</LabelsError>}
                                        </FormText>
                                    </Col>
                                </Row>



                            </Col>
                        </Row>
                        <Row className="row mt-2 mb-3">
                            <ComponenteForm className="col-lg-6">
                                <Label for="idnombre"><Labels>Nombre</Labels></Label>
                                <Input type="text" name="nombre" id="idnombre" required value={nombre} onChange={ev => setNombre(ev.target.value)} />
                            </ComponenteForm>
                            <ComponenteForm className="col-lg-6">
                                <Label for="idapellidos"><Labels>Apellidos</Labels></Label>
                                <Input type="text" name="apellidos" id="idapellidos" required value={apellidos} onChange={ev => setApellidos(ev.target.value)} />
                            </ComponenteForm>
                        </Row>
                        <Row className="row mt-2 mb-3">
                            <ComponenteForm className="col-lg-6">
                                <Label for="idemail"><Labels>Email</Labels></Label>
                                <Input type="email" name="email" id="idemail" required value={correo} onChange={ev => setCorreo(ev.target.value)} />
                            </ComponenteForm>
                            <ComponenteForm className="col-lg-6">
                                <Label for="password"><Labels>Password</Labels></Label>
                                <Input type="password" name="password" id="password" required value={contrasenya} onChange={ev => setContrasenya(ev.target.value)} />
                            </ComponenteForm>
                        </Row>
                        <Row className="row mt-2 mb-3">
                            <ComponenteForm className="col-lg-6">
                                <Label for="ididentificacion"><Labels>Identificación</Labels></Label>
                                <Row className="row mt-2 mb-3">
                                    <Col className="col-4">
                                        <Input type="select" name="nombre" id="ididentificacion">
                                            <option>DNI</option>
                                            <option>Pasaporte</option>
                                        </Input></Col>
                                    <Col className="col-8">
                                        <Input type="text" name="nombre" id="ididentificacion" />
                                    </Col>
                                </Row>
                            </ComponenteForm>
                            <ComponenteForm className="col-lg-6">
                                <Label for="idnacimiento"><Labels>Fecha de nacimiento</Labels></Label>
                                <Input type="date" name="apellidos" required="required" id="idnacimiento" value={nacimiento} onChange={ev => setNacimiento(ev.target.value)} />
                            </ComponenteForm>
                        </Row>

                        <Row><Col className="col-12">
                            <Button className="m-3" onClick={Registrarse}>Registrarse</Button>
                            <Button className="m-3" onClick={() => setVolver(!volver)}>Volver a Iniciar Sesion</Button>
                        </Col></Row>
                    </Form>
                </FondoRegistro>
            </FondoRayos>
        </>

    );

}