import React, { useState, useEffect } from "react";
import { NavLink, Link, Redirect } from "react-router-dom";
import { Container, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import styled from "styled-components";
import "bootstrap/dist/css/bootstrap.min.css";
import { withCookies } from "react-cookie";
import RayoFondo from '../Fotos/RayoBienInicio.png';

import {
  Navbar,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

import Datamap from "react-datamaps";

import Controller from "../BienvenidaController";

import "./Inicio.css";

const Pregunta_viajar = styled.h2`
text-align:center;
margin-top:20px;
font-family: 'Architects Daughter', cursive;
font-size:200%;
`;

const Aventura = styled.div`
font-family: 'Architects Daughter', cursive;
margin-top:20px;
font-size: 150%;
`

const FotoPerfilNav = styled.div`
border-radius:50%;
border:2px solid black;
width:50px;
height:50px;
display:inline-block;
background-size: cover;
background-position:center;
background-image:url(${props => props.imgSrc});
`;
  const Labels = styled.h4`
  font-family: 'Architects Daughter', cursive;
  `;

const Icono = styled.i`
color: #007bff;
&:hover{
  transform: scale(1.5);
  color: #007bff;
  cursor:pointer;
}

`;

const Inicio= (props) => {

  const [tornar,setTornar]=useState(false);

  //DEL NAVBAR

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  //Modal
  const [modal, setModal] = useState(false);
  const toggleModal = () => setModal(!modal);
  

  //Mis variables
  const [Pais, setPais] = useState();

   const [datosFotos, setDatosFotos] = useState([]);
    
  const id = props.cookies.get("Compitrueno_idUsuario");
const foto = props.cookies.get("Compitrueno_fotoUsuario");

    const [error, setError] = useState('');
  


  // const [Presupuesto,setPresupuesto]=useState("");
  // const [Fecha,setFecha]=useState("");
  const url_paisInicio = `/inicio/${Pais}`;
  const url_perfil = `/perfil/${id}`;
  const url_configuracion = `/configuracion/${id}`;
  const url_guardados = `/guardados/${id}`;


const NuevoPais=()=>{

  console.log("el nuevo pais es "+ Pais)

  Controller.getPaisNuevo(Pais)
  .then((resp)=>{
  if(resp.ok){
    console.log("Guardado de cookie")
    props.cookies.set("Compitrueno_paisElegido",resp.data[0].nombre , { path: "/" });
    props.cookies.set("Compitrueno_idPaisElegido",resp.data[0].id , { path: "/" });
  }})
  .then(()=>setTornar(true))
  .catch((err) => console.log("err login: ", err));
}
  

  
   //FOTOS
useEffect(()=>{
        Controller.getFotos(id)
            .then(data => {
                if (data.ok) {
                    setDatosFotos(data.data);
                } else {
                    setError(data.err);
                }
            })
            .catch(err => setError(err.message));

    }, [])

    const API_FOTOS = 'http://localhost:3000/img/';
   



if(tornar){
  return(<Redirect to={url_paisInicio}/>);
}

  return (
    <>

      <Navbar className="estNavbar" expand="md-right">
        <NavLink exact className="nav-link" to="/inicio">
        <img src={RayoFondo} width="60px" height="60px"/>
        </NavLink>

        <Nav className="mr" navbar>
          <UncontrolledDropdown nav >
            <DropdownToggle nav caret >
              <FotoPerfilNav  imgSrc={API_FOTOS + foto}/>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>
                <NavLink exact className="nav-link" to={url_perfil}>
                  Perfil
              </NavLink>

              </DropdownItem>
              <DropdownItem>
                <NavLink exact className="nav-link" to={url_configuracion}>
                  Configuración
              </NavLink>
              </DropdownItem>
              <DropdownItem divider />
              <DropdownItem>
                <NavLink exact className="nav-link" to={url_guardados}>
                  Guardado
              </NavLink>
              </DropdownItem>

              <DropdownItem>
                <NavLink exact className="nav-link" to="/iniciar">
                  Cerrar Sesión
              </NavLink>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
      </Navbar>


     
      <Container className=" estContainer "><Row><Col>
      <Pregunta_viajar>Próximo destino... </Pregunta_viajar>
      <Pregunta_viajar>{Pais}</Pregunta_viajar>
        <Datamap
          scope="world"
          responsive
          done={function (datamap) {
            datamap.svg.selectAll('.datamaps-subunit').on('click', function (geography) {
              setPais(geography.properties.name);
              toggleModal();
            })
          }}
          geographyConfig={{
            popupOnHover: true,
            highlightOnHover: true,
            popupTemplate: function (geo, data) {
              setPais(geo.properties.name)
            }
          }}
          fills={{
            defaultFill: "#6F0CBB"
          }}
        />

      </Col></Row></Container>
      <div>
        <Modal isOpen={modal} toggle={toggleModal}>
          <ModalHeader toggle={toggleModal}><Aventura><span>Comienza tu aventura en...</span></Aventura></ModalHeader>
          <ModalBody>
            <Pregunta_viajar>{Pais}</Pregunta_viajar>
          </ModalBody>
          <ModalFooter>
           <Icono onClick={NuevoPais} className="fa fa-paper-plane-o fa-lg" aria-hidden="true"></Icono>
          </ModalFooter>
        </Modal>
      </div>
    </>
  );

}

export default withCookies(Inicio);