import react, { useState } from "react";
import { NavLink, Link, Redirect } from "react-router-dom";
import { Container, Button, Row, Col, Input, FormText, Label } from "reactstrap";
import styled from 'styled-components';
import RayoFondo from '../Fotos/RayoBienInicio.png';
import { withCookies } from "react-cookie";
import Controller from "../BienvenidaController.js";

const Fondo = styled.div`
background-color:white;
background-image: url(${RayoFondo});
display:inline-block;
background-position:center;


width:100%;
height:100vh;
padding-top:200px;


`;
const Titulo = styled.h1`
font-family: 'Architects Daughter', cursive;
`;
const Labels = styled.h4`
font-family: 'Architects Daughter', cursive;
`;
const FondoRegistro = styled.div`

background-color:white;
border: 5px solid black;
border-radius:10px;
text-align:center;
padding:20px;

box-shadow: 0 0 .25em .25em rgba(0, 0, 0, 0.25);
`;

const LabelsError = styled.p`
font-family: 'Architects Daughter', cursive;
color: red;
font-size: 1.2em;
`;

const IniciarSesion = (props) => {
    const [correo, setCorreo] = useState("");
    const [contrasenya, setContrasenya] = useState("");
    const [tornar, setTornar] = useState(false);
    const [errorPass, setErrorPass] = useState(false);






    const Logearse = () => {
        const data = { correo, contrasenya };
        Controller.login(data)
            .then((resp) => {
                if (resp.ok) {
                    props.cookies.set("Compitrueno_emailUsuario", data.correo, { path: "/" });
                    props.cookies.set("Compitrueno_idUsuario", resp.token.usuario_id, { path: "/" });
                    props.cookies.set("token", resp.token.token, { path: "/" });
                    setTornar(true);
                } else {
                    throw new Error("El correo o contraseña son incorrectos");
                }

            })

            .then(() => {

                const id = props.cookies.get("Compitrueno_idUsuario");
                Controller.getUsuarioById(id)
                    .then(({ data }) => {
                        props.cookies.set("Compitrueno_fechaUsuario", data.nacimiento, { path: "/" });
                        props.cookies.set("Compitrueno_nombreUsuario", data.nombre, { path: "/" });
                        props.cookies.set("Compitrueno_apellidosUsuario", data.apellidos, { path: "/" });
                        props.cookies.set("Compitrueno_fotoUsuario", data.foto, { path: "/" });
                    })
            })
            .catch((err) => {

                setErrorPass(true);
                console.log("err login: ", err);
            });
    };


    if (tornar) {
        return (<Redirect to="/inicio" />)
    }


    return (
        <>
            <Fondo>

                <FondoRegistro className="container">
                    <Row className="row mt-3 mb-2 text-center" >
                        <Col>
                            <Titulo>INICIA SESIÓN</Titulo>
                        </Col>
                    </Row>
                    <Row className="row mt-3 mb-2" >

                        <Col className="col-md-6 col-12 text-center">
                            <Label for="exampleEmail"><Labels>Email:</Labels></Label>
                        </Col>
                        <Col className="col-md-6 col-12 ">
                            <Input type="email" value={correo} name="email" id="exampleEmail" placeholder="Nombre" onChange={(ev) => setCorreo(ev.target.value)} />
                        </Col>

                    </Row>

                    <Row className="row mt-3 mb-2">

                        <Col className="col-md-6 col-12 text-center">
                            <Label for="examplepassword"><Labels>Contraseña:</Labels></Label>
                        </Col>
                        <Col className="col-md-6 col-12">
                            <Input type="password" value={contrasenya} name="password" id="examplepassword" placeholder="Contraseña" onChange={(ev) => setContrasenya(ev.target.value)} />
                            <Link color="black" to="/registrarse">
                                ¿Has olvidado tu contraseña?
                                {errorPass && <LabelsError>El correo o contraseña son incorrectos</LabelsError>}
                            </Link>
                        </Col>

                    </Row>
                    <Row className="row mt-3 mb-2 mt-5">
                        <Col className="col-md-6 col-12 text-center">

                            <Button outline color="primary" onClick={Logearse}>Iniciar</Button>
                        </Col>
                        <Col className="col-md-6 col-12 text-center">
                            <Link to="./registrarse"> <Button outline color="primary">Registrarse </Button></Link>
                        </Col>
                    </Row>
                </FondoRegistro>
            </Fondo>
        </>

    );

}

export default withCookies(IniciarSesion)