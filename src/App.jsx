import React, { useState } from "react";
import { BrowserRouter, NavLink, Switch, Route } from "react-router-dom";
import { Container, Button, Row, Col } from "reactstrap";
import "bootstrap/dist/css/bootstrap.min.css";


import Bienvenida from "./Inicio/Bienvenida.jsx";
import IniciarSesion from "./Inicio/IniciarSesion.jsx";
import Registrarse from "./Inicio/Registrarse.jsx";

import Inicio from "./Inicio/Inicio.jsx";
import PaisInicio from "./Contenido/PaisInicio.jsx";
import Viajeros from "./Contenido/Viajeros.jsx";
import Anfitriones from "./Contenido/Anfitrion.jsx";
import Reviews from "./Contenido/Reviews.jsx";

import PubliViajeros from "./Contenido/Post/PubliViajeros.jsx";
import PubliAnfitrion from "./Contenido/Post/PubliAnfitrion.jsx";
import PubliFoto from "./Contenido/Post/PubliFoto.jsx";
import PubliReviews from "./Contenido/Post/PubliReview";

import Perfil from "./Perfil/Perfil.jsx";
import Configuracion from "./Perfil/Configuracion.jsx";
import Guardados from "./Perfil/Guardados.jsx";
import PerfilOtro from "./Contenido/PerfilOtro.jsx";
import Contacto from "./Contenido/Contacto.jsx";


import NotFound from "./Elementos/P404.jsx";

import Mundo from "./Elementos/mundo.jsx";

import Footer from './Elementos/Footer.jsx';

//import "./Todo.css";

export default () => {
  return (
    <BrowserRouter>
      <Container fluid>
   
        <Switch>
          {/* RUTAS BIENVENIDA */}
          <Route exact path="/" component={Bienvenida} />
          <Route path="/iniciar" component={IniciarSesion} />
          <Route path="/registrarse" component={Registrarse} />

          

          {/* RUTAS INICIO */}
          <Route exact path="/inicio" component={Inicio} />
          <Route exact path="/inicio/:pais" component={PaisInicio} />
          <Route exact path="/inicio/:pais/viajeros" component={Viajeros} />
          <Route exact path="/inicio/:pais/anfitriones" component={Anfitriones} />
          <Route exact path="/inicio/:pais/reviews" component={Reviews} />

          {/* RUTAS POSTEAR COSAS */}

          <Route path="/inicio/:pais/viajeros/publicacion" component={PubliViajeros} />
          <Route path="/inicio/:pais/anfitriones/publicacion" component={PubliAnfitrion} />
          <Route path="/inicio/:pais/reviews/publicacion" component={PubliReviews} />
          <Route path="/perfil/:id/foto" component={PubliFoto} />

          <Route path="/inicio/perfil/:id1" component={PerfilOtro} />

          <Route exact path="/contacto" component={Contacto} />




          {/* RUTAS DEL PERFIL */}
          <Route  exact path="/perfil/:id" component={Perfil} />
          <Route path="/configuracion/:id" component={Configuracion} />
          <Route path="/guardados/:id" component={Guardados} />
          

          {/* OTRAS RUTAS */}
          <Route path="/mundo" component={Mundo} />
          <Route component={NotFound} />


        </Switch>
        <Footer/>
      </Container>
     
    </BrowserRouter>

  );
}


