import axios from "axios";

const api_url = 'http://localhost:3000';

export default class Controller {

    // Editar Perfil

    static editarPerfil = (item) => {
        const jsonPerfilEditado = JSON.stringify(item);
        const opcionesFetch = {
            method: "PUT",
            body: jsonPerfilEditado,
            headers: { 'Content-Type': 'application/json' },
        }
    
        fetch(api_url + "/usuarios/" +item.id, opcionesFetch)
            .then(resp => {
                console.log("perfil editado:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    
    }

    //REGISTRARSE 

    static nuevoUsuario = async (data) => {
        await axios.post(api_url + "/usuarios", data).then((res) => res.statusText);
    }

    static addUsuario = (item) => {
        const jsonUsuario = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonUsuario,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/usuarios", opcionesFetch)
            .then(resp => {
                console.log("nueva viajera:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));

    }

    //LOGIN

    static login = (data) => {
        return new Promise((ok, nok) => {

            fetch(api_url + '/usuarios/login', {
                method: 'POST',
                headers: new Headers({ 'Content-Type': 'application/json' }),
                body: JSON.stringify(data)
            })
                .then(resp => resp.json())
                .then(resp => {
                    if (resp.ok === false) {
                        nok(resp);
                    } else {
                        return resp.data;
                    }
                })
                .then(token => {
                    if (token) {
                        ok({ ok: true, token });
                    } else {
                        nok({ ok: false, err: "no token" })
                    }
                })
                .catch(err => nok({ ok: false, err }));
        })
    }

    // LOGOUT

    static logout = (token) => {
        return new Promise((ok, nok) => {
        fetch(api_url + "/usuarios/logout", {
          method: "DELETE",
          headers: new Headers({ "Content-Type": "application/json" }),
          body: JSON.stringify({ token }),
        })
          .then((res) => {
            ok({ok:true})
          })
          .catch((err) => {
              nok({ok: false, err})
          });
      });
    }



    //GET A UN USUARIO POR ID

    static getUsuarioById = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + '/usuarios/' + id)
                .then(data => data.json())
                .then(contacto => {
                    resuelve(contacto);
                })
                .catch(err => {
                    falla(err);
                });
        };

        return new Promise(promesa);
    }

    //MANDO NOMBRE Y PASSWORD Y ME DEVUELVE TRUE O FALSE

    static getVerificacion = (nombre, password) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/login")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);

    } 

    //LOGIN

    static login = (data) => {
        return new Promise((ok, nok) => {

            fetch(api_url + '/usuarios/login', {
                method: 'POST',
                headers: new Headers({ 'Content-Type': 'application/json' }),
                body: JSON.stringify(data)
            })
                .then(resp => resp.json())
                .then(resp => {
                    if (resp.ok === false) {
                        nok(resp);
                    } else {
                        return resp.data;
                    }
                })
                .then(token => {
                    if (token) {
                        ok({ ok: true, token });
                    } else {
                        nok({ ok: false, err: "no token" })
                    }
                })
                .catch(err => nok({ ok: false, err }));
        })
    }

    // LOGOUT

    static logout = (token) => {
        return new Promise((ok, nok) => {
        fetch(api_url + "/usuarios/logout", {
          method: "DELETE",
          headers: new Headers({ "Content-Type": "application/json" }),
          body: JSON.stringify({ token }),
        })
          .then((res) => {
            ok({ok:true})
          })
          .catch((err) => {
              nok({ok: false, err})
          });
      });
    }

    //GET A UN USUARIO POR ID

    static getUsuarioById = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + '/usuarios/' + id)
                .then(data => data.json())
                .then(contacto => {
                    resuelve(contacto);
                })
                .catch(err => {
                    falla(err);
                });
        };

        return new Promise(promesa);
    }

    //INICIO PAIS - RECIBE TODOS LOS PAISES

    static getPais = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/paises")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    };


    //INICIO PAIS - RECIBE ID Y PAIS - SI EL PAIS NO EXISTE LO CREA

    static getPaisNuevo = (pais) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/paises/" + pais)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                    console.log("getPais done " + data)
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //INICIO PAIS - RECIBE LAS EXPERIENCIAS DEL LOS PAISES

    static getReviewsPaises = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/experiencias")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //INICIO PAIS - RECIBE LAS EXPERIENCIAS DE UN PAIS ESPECIFICO

    static getReviewsPais = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/experiencias/pais/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //VIAJERAS - RECIBE TODAS LAS VIAJERAS

    static getViajeras = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/viajeros")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //PUBLICACION VIAJERA (SE NECESITA LA FECHA, PRESUPUESTO, DESCRIPCION, EL ID DEL PAIS Y ID DEL USUARIO)

    static addViajera = (item) => {
        const jsonViajera = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonViajera,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/viajeros", opcionesFetch)
            .then(resp => {
                console.log("nueva viajera:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));

    }

    //ANFITRIONAS - RECIBE TODAS LAS ANFITRIONAS

    static getAnfitriones = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/anfitriones")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //PUBLICACION ANFITRIONA (SE NECESITA LA DESCRIPCION, CIUDAD, EL ID DEL PAIS Y ID DEL USUARIO)

    static addAnfitriona = (item) => {
        const jsonAnfitriona = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonAnfitriona,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/anfitriones", opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error al añadir tu publicación", err));

    }

    //----- REVIEWS -----

    //REVIEWS - RECIBE TODAS LAS EXPERIENCIAS

    static getExperiencias = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/experiencias")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //REVIEWS - RECIBE TODAS LAS EXPERIENCIAS QUE EL USUARIO HA HECHO

    static getExperienciasById = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/experiencias/user/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //EDITAR UNA REVIEW 

    static EditarReview = (item) => {
        const jsonEditReview = JSON.stringify(item);
        const opcionesFetch = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: jsonEditReview
        }

        fetch(api_url + "/experiencias/" + item.id, opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }

    //---- PERFIL ----

    //GET MIS FOTOS(id): RECIBE LAS FOTOS QUE HA SUBIDO EL USUARIO

    static getFotos = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/fotos/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    static getFotosAll = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/fotos")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //RECIBE LOS COMENTARIOS QUE EL USUARIO HA RECIBIDO DEVUELVE NOMBRE, NACIMIENTO, ESTRELLITAS FOTO, TEXTO
    static getComentRecibidos = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/usuarios/coment/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }


    //GET DE LAS PUBLICACIONES QUE HAS PUBLICADO COMO VIAJERA 

    static getMyPostViajera = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/viajeros/usuario/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //GET DE LAS PUBLICACIONES QUE HAS PUBLICADO COMO ANFITRIONA 

    static getMyPostAnfitriona = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/anfitriones/usuario/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //---- FOTOS ----

    //POST NUEVA FOTO (SE NECESITA LA URL DE LA FOTO, EL ID DEL PAIS Y ID DEL USUARIO)

     static nuevaFoto = async (data) => {
        await axios.post(api_url + "/fotos", data).then((res) => res.statusText);
    }
    static newFoto = (item) => {
        const jsonNewFoto = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonNewFoto,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/fotos", opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));

    }

    //BORRA UNA FOTO POR EL ID DE LA FOTO

    static borraFoto = (id) => {
        const opcionesFetch = {
            method: "DELETE",
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/fotos/" + id, opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }

    //---- PUBLICACIONES ----

    //EDITA TU PUBLICACION DE VIAJERA.  NECESARIO ID DEL VIAJERO

    static EditarPublicacionViaje = (item) => {
        const jsonPubliViaje = JSON.stringify(item);
        const opcionesFetch = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: jsonPubliViaje
        }

        fetch(api_url + "/viajeros/" + item.id, opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }

    //GET PARA CONSEGUIR LOS ID DE LA TABLA VIAJERO

    static getIdTablaViajero = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/viajeros/viajes")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //BORRA UN POST DE VIAJERA POR EL ID DE LA TABLA VIAJERO (NECESARIO ID DEL POST)

    static borraPostViajera = (id) => {
        const opcionesFetch = {
            method: "DELETE",
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/viajeros/" + id, opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }

    //EDITA TU PUBLICACION DE ANFITRIONA. (NECESARIO ID DEL ANFITRION)

    static EditarPublicacionAnfitrion = (item) => {
        const jsonPubliAnfi = JSON.stringify(item);
        const opcionesFetch = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: jsonPubliAnfi
        }

        fetch(api_url + "/anfitriones/" + item.id, opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }

    //GET PARA CONSEGUIR LOS ID DE LA TABLA ANFITRION

    static getIdTablaAnfitrion = () => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/anfitriones/anfitrion")
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //BORRA UN POST DE ANFITRION POR EL ID DE LA TABLA ANFITRION (NECESARIO ID DEL POST)

    static borraPostAnfitriona = (id) => {
        const opcionesFetch = {
            method: "DELETE",
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/anfitriones/" + id, opcionesFetch)
            .then(resp => {
                console.log("nueva anfitriona:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }

    //GET POR ID DE VISTA_VIAJEROS_GUARDADOS 

    static getViajerosGuardados = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/viajeros/guardados/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //POST A VIAJERAS GUARDADAS

    static guardaViajera = (item) => {
        const jsonGuardaViajera = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonGuardaViajera,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/viajeros/guardados", opcionesFetch)
            .then(resp => {
                console.log("anfitriona guardada:", resp)
            })
            .catch(err => console.log("error no se ha guardado la anfitriona", err));

    }

    //GET POR ID DE VISTA_ANFITRIONES_GUARDADOS 

    static getAnfitrionasGuardadas = (id) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + "/anfitriones/guardados/" + id)
                .then(data => data.json())
                .then(data => {
                    resuelve(data);
                })
                .catch(err => {
                    falla(err);
                });

        };
        return new Promise(promesa);
    }

    //POST A ANFITRIONaS GUARDADAS

    static guardaAnfitriona = (item) => {
        const jsonGuardaAnfitrion = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonGuardaAnfitrion,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/anfitriones/guardados", opcionesFetch)
            .then(resp => {
                console.log("anfitriona guardada:", resp)
            })
            .catch(err => console.log("error no se ha guardado la anfitriona", err));

    }
}

